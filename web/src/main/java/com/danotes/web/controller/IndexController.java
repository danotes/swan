package com.danotes.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class IndexController {

    private final static Logger LOGGER = LoggerFactory.getLogger(IndexController.class);

    @GetMapping(value = {"equipment"})
    public String equipment(RedirectAttributes attributes, ModelMap map) {
        System.out.println("============equipment.html==============");
        return "equipment";
    }

    @GetMapping(value = {"hospital"})
    public String hospital(RedirectAttributes attributes, ModelMap map) {
        System.out.println("============hospital.html==============");
        return "hospital";
    }

    @GetMapping(value = {"user"})
    public String user(RedirectAttributes attributes, ModelMap map) {
        System.out.println("============user.html==============");
        return "user";
    }

    @GetMapping(value = {"system"})
    public String system(RedirectAttributes attributes, ModelMap map) {
        System.out.println("============system.html==============");
        return "system";
    }

    @GetMapping(value = {"log"})
    public String log(RedirectAttributes attributes, ModelMap map) {
        System.out.println("============log.html==============");
        return "equipment";
    }

}
