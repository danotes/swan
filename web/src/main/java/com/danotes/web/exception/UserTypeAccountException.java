package com.danotes.web.exception;

import org.apache.shiro.authc.DisabledAccountException;

public class UserTypeAccountException extends DisabledAccountException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserTypeAccountException() {
        super();
    }

}
