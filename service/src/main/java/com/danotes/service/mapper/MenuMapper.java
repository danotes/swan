package com.danotes.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danotes.core.entity.Menu;
import com.danotes.core.entity.vo.ShowMenuVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<ShowMenuVo> selectShowMenuByUser(Map<String, Object> map);

    List<Menu> getMenus(Map<String, Object> map);
}