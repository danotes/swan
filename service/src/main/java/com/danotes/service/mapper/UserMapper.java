package com.danotes.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danotes.core.entity.Role;
import com.danotes.core.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;
import java.util.Set;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    User selectUserByMap(Map<String, Object> map);

    void saveUserRoles(@Param("userId") String id, @Param("roleIds") Set<Role> roles);

    void dropUserRolesByUserId(@Param("userId") String id);
}