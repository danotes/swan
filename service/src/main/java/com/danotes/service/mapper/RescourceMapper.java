package com.danotes.service.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.danotes.core.entity.Rescource;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RescourceMapper extends BaseMapper<Rescource> {
}
