package com.danotes.core.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.danotes.core.base.TreeEntity;

@TableName("sys_group")
public class Group extends TreeEntity<Group> {
    private static final long serialVersionUID = 1L;
}
